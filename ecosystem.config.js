module.exports = {
    apps: [{
        name: 'jacareBot',
        script: './build/main.js',
        instances: 1,
        exec_mode: "cluster",
        autorestart: true,
        max_restarts: 50,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production'
        },
        log_date_format: "DD-MM-YYYY HH:mm Z”"
    }]
};

