# Jacaré Bot

Twitter bot to daily inform data about COVID-19 vaccination in Brazil and in the World.

Alongside with the vaccination numbers there is also an ironic statistic about the number of people that turned into alligators because of the vaccine in order to criticize the [brazilian president claim](https://www.youtube.com/watch?v=PcfAjTR8UgM).

Bot made with Node.js, using typescript, the Twitter API and  the Puppeteer module. Hosted with Amazon AWS.

[Twitter Profile Link](https://twitter.com/jacare_bot)

## Sources:
 -  Brazil: [coronavirusbra1.github.io](https://coronavirusbra1.github.io)
 - World: [ourworldindata.org](https://ourworldindata.org/covid-vaccinations)