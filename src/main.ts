process.env.TZ = 'America/Sao_Paulo';

import puppeteer from 'puppeteer';
import { CovidData } from './model/covidData';
import { Crawler } from './utils/crawler';
import { Fetcher } from './utils/fetcher';
import { brazilDataFromHtml, worldDataFromCsv } from './utils/parser';
import dotenv from 'dotenv';
import { Bird } from './utils/bird';
import schedule from 'node-schedule';

dotenv.config();

const brazilDataUrl = process.env.BRAZIL_DATA_URL || 'invalid';
const worldDataUrl = process.env.WORLD_DATA_URL || 'invalid';

const getDataAndTweet = async () => {
    const browser = await puppeteer.launch();
    const bird = new Bird();

    const crawler = new Crawler(browser);

    const brData = brazilDataFromHtml(await crawler.getPageHtml(brazilDataUrl));
    const worldData = await worldDataFromCsv(await Fetcher.fetchUrlData(worldDataUrl));

    const covidData = new CovidData(brData, worldData);

    bird.tweet(covidData.toString());

    await browser.close();
};

if (process.env.NODE_ENV === 'production') {
    const job = schedule.scheduleJob('0 10 * * *', getDataAndTweet);
    console.log(job.nextInvocation().toString()); // First invocation
}else{
    getDataAndTweet();   
}