import puppeteer, { Browser } from 'puppeteer';

export class Crawler {
    private browser: Browser

    constructor(browser: Browser) {
        this.browser = browser;
    }

    async getPageHtml(pageUrl: string): Promise<string> {
        const page = await this.browser.newPage();
        await page.goto(pageUrl, { waitUntil: 'networkidle2' });
        const html = await page.content();
        await page.close()
        return html;
    }
}