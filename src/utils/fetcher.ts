import fetch from 'node-fetch'

export class Fetcher{
    static async fetchUrlData(url: string) : Promise<string>{
        const response = await fetch(url);
        return response.text();
    }
}