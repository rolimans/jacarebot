import cheerio from 'cheerio';
import neatCsv from 'neat-csv';
import { LocationCovidData } from "../model/covidData";

export const brazilDataFromHtml = (htmlData: string): LocationCovidData => {
    const totalBrPopulation = parseInt(process.env.TOTAL_BR_POPULATION || '1');
    const $ = cheerio.load(htmlData);
    const vaccinated = parseInt($('.kpimetric:contains("1ª Dose")').find('.valueLabel').text().trim().replace(/\./g, ''));
    const fullyVaccinated = parseInt($('.kpimetric:contains("2ª Dose")').find('.valueLabel').text().trim().replace(/\./g, ''));
    const percentageVaccinated = ((vaccinated / totalBrPopulation) * 100);
    const percentageFullyVaccinated = (fullyVaccinated / totalBrPopulation) * 100;

    return { vaccinated, fullyVaccinated, percentageFullyVaccinated, percentageVaccinated };
};

export const worldDataFromCsv = async (csvData: string): Promise<LocationCovidData> => {
    const csv = await neatCsv(stripLastData(csvData));
    const worldData = csv[0];
    const vaccinated = parseInt(worldData.people_vaccinated);
    const fullyVaccinated = parseInt(worldData.people_fully_vaccinated);
    const percentageVaccinated = parseFloat(worldData.people_vaccinated_per_hundred);
    const percentageFullyVaccinated = parseFloat(worldData.people_fully_vaccinated_per_hundred);

    return { vaccinated, fullyVaccinated, percentageFullyVaccinated, percentageVaccinated };
};

const stripLastData = (data: string): string => {
    const worldRegex = /(^World.*$)/gm;
    const header = data.split('\n')[0];
    const worldLines = data.match(worldRegex);
    const importantLines = [
        header,
        worldLines ? worldLines[worldLines?.length - 1] : 'invalid'
    ];

    return importantLines.join('\n');
};