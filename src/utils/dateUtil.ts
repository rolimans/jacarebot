export const dateString = (date: Date): string => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // Starts at 0
    const day = date.getDate();
    return leadingZero(day) + '/' + leadingZero(month) + '/' + year;
};

const leadingZero = (num: number | string): string => {
    return ('0' + num).slice(-2);
};