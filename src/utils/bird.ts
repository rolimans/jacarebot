import Twitter from "twitter";

export class Bird {
    private client: Twitter;

    constructor() {
        const access_token_key = process.env.TWITTER_ACESS_TOKEN_KEY?.toString() || 'invalid';
        const access_token_secret = process.env.TWITTER_ACESS_TOKEN_SECRET?.toString() || 'invalid';
        const consumer_key = process.env.TWITTER_CONSUMER_KEY?.toString() || 'invalid';
        const consumer_secret = process.env.TWITTER_CONSUMER_SECRET?.toString() || 'invalid';

        this.client = new Twitter({
            access_token_key,
            access_token_secret,
            consumer_key,
            consumer_secret,
        });
    }

    async tweet(text: string) {
        if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'imediate') {
            try {
                console.log(await this.client.post('statuses/update', { status: text }));
            } catch (e) {
                console.error(e);
                console.error(text);
                console.error(text.length);
            }
        }else{
            console.log(text);
        }
    }
}