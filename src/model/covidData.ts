import { dateString } from "../utils/dateUtil";

export interface LocationCovidData {
    vaccinated: number,
    fullyVaccinated: number,
    percentageVaccinated: number,
    percentageFullyVaccinated: number
}

export class CovidData {
    readonly brazilData: LocationCovidData;
    readonly worldData: LocationCovidData;
    readonly totalAlligators

    constructor(brazilData: LocationCovidData, worldData: LocationCovidData) {
        this.brazilData = brazilData;
        this.worldData = worldData;
        this.totalAlligators = 0;
    }

    toString(): string {

        const today = dateString(new Date());
        let str = `🦠 Status da Vacinação contra COVID-19 - 📅 ${today}\n`;

        str += `\n💉 Pessoas que iniciaram a vacinação:\n\n`;

        str += `🇧🇷: ${this.brazilData.vaccinated.toLocaleString('pt')} (${this.brazilData.percentageVaccinated.toFixed(2).replace('.', ',')}%)\n`;
        str += `🌎: ${this.worldData.vaccinated.toLocaleString('pt')} (${this.worldData.percentageVaccinated.toFixed(2).replace('.', ',')}%)\n`;

        str += `\n✅ Pessoas que tomaram todas as doses:\n\n`;
        
        str += `🇧🇷: ${this.brazilData.fullyVaccinated.toLocaleString('pt')} (${this.brazilData.percentageFullyVaccinated.toFixed(2).replace('.', ',')}%)\n`;
        str += `🌎: ${this.worldData.fullyVaccinated.toLocaleString('pt')} (${this.worldData.percentageFullyVaccinated.toFixed(2).replace('.', ',')}%)\n`;

        str+= `\n🐊Pessoas que viraram jacaré: ${this.totalAlligators}`

        return str;
    }
}